
name := "sbt-docker-gitlab-ci-example"

version := "0.1"

scalaVersion := "2.13.2"
lazy val dockerJdkVersion = "14.0.1"
lazy val dockerImageVersion = sys.env.getOrElse("IMAGE_VERSION", "snapshot")
dockerRepository := Some("registry.gitlab.com")
dockerUsername := Some("cucumissativus") //note that docker repository must be lowercase
packageName in Docker := "sbt-docker-gitlab-ci-example"
version in Docker := dockerImageVersion
dockerBaseImage := s"azul/zulu-openjdk-alpine:$dockerJdkVersion"

enablePlugins(JavaAppPackaging, DockerPlugin)